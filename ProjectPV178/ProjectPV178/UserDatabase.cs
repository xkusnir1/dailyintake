﻿using System;
using System.IO;
using System.Threading.Tasks;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;

namespace ProjectPV178
{
    public class UserDatabase
    {
        string FilePath  = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\DailyIntake";

        readonly SQLiteAsyncConnection _database;

        public UserDatabase()
        {
            Directory.CreateDirectory(FilePath);
            _database = new SQLiteAsyncConnection(FilePath + @"\Users.db3");
            _database.CreateTableAsync<User>().Wait();
            _database.CreateTableAsync<Meal>().Wait();
            _database.CreateTableAsync<Ingredient>().Wait();
        }
        
        public bool CheckUser(string login)
        {
            var temp = _database.Table<User>().Where(u => u.Login.Equals(login)).CountAsync();
            temp.Wait();
            return temp.Result != 0;
        }
        public bool CheckUser(User user)
        {
            return CheckUser(user.Login);
        }
        
        public Task SaveUser(User user)
        {
            return _database.InsertOrReplaceWithChildrenAsync(user);
        }

        public Task<User> GetUser(string login)
        {
            return _database.GetWithChildrenAsync<User>(login);
        }

        public Task<int> DeleteUser(User user)
        {
            return _database.DeleteAsync(user);
        }
        
    }
}