﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace ProjectPV178
{
    public class Parser
    {
        
        private IngredientDatabase _ingredientDatabase;
        public Parser(IngredientDatabase database)
        {
            _ingredientDatabase = database;
        }


        public DateTime ParseDay(string str)
        {
            DateTime result;
            if (!DateTime.TryParseExact(str, "dd/MM/yyyy",
                CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                Console.Out.WriteLine("Couldn't parse date: " + str + "\n Date was set to today.");
                result = DateTime.Today;
            }
            return result;
        }
    }
}