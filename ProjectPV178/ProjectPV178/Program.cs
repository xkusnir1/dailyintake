﻿using System;
using SQLite;

namespace ProjectPV178
{
    internal class Program
    {
        
        
        public static void Main(string[] args)
        {
            ConsoleInterface _interface = new ConsoleInterface();
            while (!_interface.ExitFlag)
            {
                _interface.Login();
                if (_interface.ExitFlag) break;
                _interface.Home();
            }
        }
    }
}