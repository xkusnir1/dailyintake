﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProjectPV178
{
    public class IngredientDatabase
    {
        private SortedDictionary<string, List<Ingredient>> _dictionary = new SortedDictionary<string, List<Ingredient>>();

        public IngredientDatabase()
        {
            Console.Out.WriteLine("Loading database...");
            Console.Out.WriteLine();
            LoadIngredients();
            Console.Out.WriteLine("Done");
            
        }

        public void LoadIngredients()
        {
            /* I've decided to put the file on desktop for purposes of this project, normally it would be placed
             somewhere more "discrete" (like application installation folder) */
            var reader = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +  @"\food.csv");
            
            /* take header line from the table */
            string line = reader.ReadLine();
            
            while (!reader.EndOfStream)
            {
                line = reader.ReadLine();
                bool badParseFlag = false;
                
                if (!String.IsNullOrWhiteSpace(line))
                {
                    line = line.ToLower();
                    var data = line.Split('"');
                    string category = data[0].Trim(',');
                    string name = data[1];

                    var values = data[2].Trim(',').Split(',');
                    double[] valuesParsed = new double[4];

                    for (int i = 0; i < 4; i++) 
                    {

                        if (!Double.TryParse(values[i], out valuesParsed[i]))
                        {
                            badParseFlag = true;
                            break;
                        }
                    }

                    if (!badParseFlag)
                    {
                        var ingredient = new Ingredient(name, valuesParsed[2], valuesParsed[1], valuesParsed[3],
                            valuesParsed[0]);
                        if (!_dictionary.ContainsKey(category))
                        {
                            _dictionary.Add(category, new List<Ingredient>());
                        }

                        _dictionary[category].Add(ingredient);
                    }
                }
            }
        }

        public bool CheckCategory(string category)
        {
            return _dictionary.ContainsKey(category);
        }

        public void PrintCategories()
        {
            foreach (var category in _dictionary)
            {
                Console.Out.WriteLine(category.Key);
            }
        }

        public void PrintIngredients(string category)
        {
            int counter = 0;
            foreach (var ingredient in _dictionary[category])
            {
                counter++;
                Console.Out.Write(counter + ". ");
                Console.Out.WriteLine(ingredient.Name);
            }
        }

        public bool CheckIngredient(string category, string name)
        {
            if (CheckCategory(category))
            {
                foreach (var ingredient in _dictionary[category])
                {
                    if (ingredient.Name.Equals(name))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public Ingredient GetIngredient(string category, int index)
        {
            if (!CheckCategory(category))
            {
                Console.Out.WriteLine("Category does not exist.");
                return new Ingredient();
            }else if (_dictionary[category].Count <= index)
            {
                Console.Out.WriteLine("Index in category out of bounds.");
                return new Ingredient();
            }
            return _dictionary[category][index];
        }

    }
}