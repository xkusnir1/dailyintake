﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Attributes;
using SQLiteNetExtensionsAsync;

namespace ProjectPV178
{
    
    public class Ingredient
    {
        public string Name { get; set; }

        public int Grams { get; set; } = 0;

        public double Protein { get; set; } = 0;
        public double Carbs { get; set; } = 0;
        public double Fats { get; set; } = 0;

        public double Calories { get; set; } = 0;
        
        public Ingredient()
        {
            Name = "undefined";

        }
        public Ingredient(string name, double prot, double carbs, double fats, double calories)
        {
            Name = name;
            Protein = prot;
            Carbs = carbs;
            Fats = fats;
            Calories = calories;
        }

        public Ingredient Add(Ingredient ingredient)
        {
            ingredient.Calories += Calories;
            ingredient.Fats += Fats;
            ingredient.Carbs += Carbs;
            ingredient.Protein += Protein;
            return ingredient;
        }

        public void Divide(double num)
        {
            Calories /= num;
            Fats /= num;
            Carbs /= num;
            Protein /= num;
        }
    }
}