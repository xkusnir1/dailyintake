﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using SQLiteNetExtensions.Attributes;
using SQLiteNetExtensionsAsync;

namespace ProjectPV178
{
    public class User
    {
        [PrimaryKey]
        public string Login { get; set; }
        
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<Meal> Meals { get; set; } = new List<Meal>();
        
        
        /* This is to fix this error: https://bitbucket.org/twincoders/sqlite-net-extensions/issues/91/sqlitenetsqliteexception-near-where-syntax */
        public string foo { get; set; }


        public User()
        {
            Login = "";
        }
        public User(string login)
        {
            Login = login;
        }

        public int DaysOnRecord()
        {
            var set = new HashSet<DateTime>();

            foreach (var meal in Meals)
            {
                set.Add(meal.Date);
            }

            return set.Count;
        }
    }
}