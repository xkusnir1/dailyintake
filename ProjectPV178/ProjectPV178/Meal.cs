﻿using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace ProjectPV178
{
    public class Meal
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        
        /* List of what the meal contained, doesn't get saved in the database (I wasn't able to get it to work,
         didn't find an example where children of children worked, maybe SQLiteNetExtensions doesn't support it), 
         only for runtime calculations */
        [Ignore]
        public List<Ingredient> Ingredients { get; set; } = new List<Ingredient>();

        public string Name { get; set; }

        /* Total nutrient content of the meal. IMO ugly solution but i just wanted to get it to work somehow. */
        public double Protein { get; set; } = 0;
        public double Carbs { get; set; } = 0;
        public double Fats { get; set; } = 0;
        public double Calories { get; set; } = 0;

        /* When was the meal consumed */
        public DateTime Date { get; set; } = DateTime.Today;

        [ForeignKey(typeof(User))] 
        public string UserLogin { get; set; } = "";
        [ManyToOne(CascadeOperations = CascadeOperation.All)]
        public User User { get; set; } = new User();

        public void AddIngredient(Ingredient i, int amount)
        {
            var ingredient = i;
            ingredient.Grams = amount;
            Ingredients.Add(ingredient);
        }

        
        public Ingredient MealToIngredient()
        {
            var result = new Ingredient();
            result.Calories = Calories;
            result.Carbs = Carbs;
            result.Fats = Fats;
            result.Protein = Protein;
            return result;
        }

        /* Only used initially when adding a meal, if used after being loaded from db it shouldn't change
         anything because Ingredients should be empty. */
        public void SetMealNutrients()
        {
            foreach (var ingredient in Ingredients)
            {
                Calories += ingredient.Calories * ingredient.Grams / 100;
                Carbs += ingredient.Carbs * ingredient.Grams / 100;
                Fats += ingredient.Fats * ingredient.Grams / 100;
                Protein += ingredient.Protein * ingredient.Grams / 100;
            }
        }
        
    }
}