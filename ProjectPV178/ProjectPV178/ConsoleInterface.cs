﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ProjectPV178
{
    
    /* This class is a bit too big (400 lines), but i'm not sure how would i divide it and most of it is
     switches for menus and print functions. */
    public class ConsoleInterface
    {
        static IngredientDatabase _ingredientDatabase = new IngredientDatabase();
        UserDatabase _userDatabase = new UserDatabase();
        private Parser _parser = new Parser(_ingredientDatabase);
        private User _currUser;
        public bool ExitFlag = false;
        public Task Saving;

        public void Login()
        {
            
            Console.Out.WriteLine("Welcome to DailyIntake, do you have an account? y/n");
            Console.Out.WriteLine("If you wish to quit enter 'q'.");
            string key = Console.ReadLine();
            
            while (key == null || !Regex.IsMatch(key, @"[nNyYqQ]"))
            {
                Console.Out.WriteLine("Wrong input, try again.\n Do you have an account? y/n");
                Console.Out.WriteLine("If you wish to quit enter 'q'.");
                key = Console.ReadLine();
            }

            key = key.ToLower();
            
            if (key.Equals("n"))
            {
                _currUser = MakeNewAcc();
            }
            else if(key.Equals("y"))
            {
                Console.Out.WriteLine("Enter your login.");
                string login = Console.ReadLine();
                while (!_userDatabase.CheckUser(login))
                {
                    Console.Out.WriteLine("User not found, try again.");
                    login = Console.ReadLine();
                }
                var temp = _userDatabase.GetUser(login);
                Console.Out.WriteLine("Welcome " + login + ", loading your data.");
                temp.Wait();
                _currUser = temp.Result;
            }else 
            {
                ExitFlag = true;
                return;
            }
            _currUser.Meals.Sort((meal1, meal2) => meal1.Date.CompareTo(meal2.Date));
            Saving = _userDatabase.SaveUser(_currUser);
        }


        
        public User MakeNewAcc()
        {
            Console.Out.WriteLine("Enter new login.");
            string login = Console.ReadLine();
            while (_userDatabase.CheckUser(login))
            {
                Console.Out.WriteLine("Login taken, try a different login.");
                login = Console.ReadLine();
            }
            return new User(login);
        }
        
        public void Home()
        {
            string input = "";
            while (input != null && !input.Equals("5") && !ExitFlag)
            {
                HomeCommands();
                input = Console.ReadLine();
        
                switch (input)
                {
                    case "1":
                        AddMealMenu();
                        break;
                    case "2":
                        PrintMealsMenu();
                        break;
                    case "3":
                        CalculateNutrientsMenu();
                        break;
                    case "4":
                        RemoveMealsMenu();
                        break;
                }
            }
            Save();
            Saving.Wait();
        }

        public void HomeCommands()
        {
            Console.Out.WriteLine("Enter a number to enter further menus.");
            Console.Out.WriteLine("1 - Add a meal.");
            Console.Out.WriteLine("2 - Print my meals.");
            Console.Out.WriteLine("3 - Calculate nutrients.");
            Console.Out.WriteLine("4 - Remove meals.");
            Console.Out.WriteLine("5 - Save and logout.");
        }
        
        
        public void PrintMealsCommands()
        {
            Console.Out.WriteLine("Enter a number.");
            Console.Out.WriteLine("1 - Print meals for a set day.");
            Console.Out.WriteLine("2 - Print meals for a range of days.");
            Console.Out.WriteLine("3 - Print all your stored meals.");
            Console.Out.WriteLine("4 - Print yesterday's meals.");
            Console.Out.WriteLine("5 - Print today's meals");
            Console.Out.WriteLine("6 - Quit this menu.");

        }

        public void CalculateNutrientsCommands()
        {
            Console.Out.WriteLine("Enter a number.");
            Console.Out.WriteLine("1 - Calculate nutrients for a set day.");
            Console.Out.WriteLine("2 - Calculate daily average nutrients intake for a range of days.");
            Console.Out.WriteLine("3 - Calculate your all time daily average.");
            Console.Out.WriteLine("4 - Calculate yesterday's nutrients.");
            Console.Out.WriteLine("5 - Calculate today's nutrients.");
            Console.Out.WriteLine("6 - Quit this menu.");
        }

        public void RemoveMealsCommands()
        {
            Console.Out.WriteLine("Enter the number of the meal you wish to delete" +
                                  " or enter 'q' to quit this menu.");
        }
        

        public void AddMealMenu()
        {
            Console.Out.WriteLine("Enter the date (dd/MM/yyyy) of the meal, or press enter if the meal was today.");
            string inputDate = Console.ReadLine();
            Meal meal = new Meal();
            if (inputDate != null && inputDate.Equals(""))
            {
                meal.Date = DateTime.Today;
            }
            else if(inputDate != null)
            {
                meal.Date = _parser.ParseDay(inputDate);
            }
            else
            {
                Console.Out.WriteLine("Something went wrong.");
                return;
            }
            
            Console.Out.WriteLine("Enter the name of the meal.");
            meal.Name = Console.ReadLine();
            Console.Out.WriteLine("Next you will input ingredients of the meal to calculate nutrient content.");
                
            string inputCategory = "";
            Console.Out.WriteLine("Enter \"print\" to print all categories or enter a category. When you are done " +
                                      "adding ingredients to meal, enter \"done\".");
            inputCategory = Console.ReadLine();
            while (inputCategory != null && !inputCategory.Equals("done"))
            {
                if (inputCategory.Equals("print"))
                {
                    _ingredientDatabase.PrintCategories();
                }
                else if(_ingredientDatabase.CheckCategory(inputCategory))
                {
                    _ingredientDatabase.PrintIngredients(inputCategory);
                    Console.Out.WriteLine("Enter the number of ingredient.");
                    string ingredient = Console.ReadLine();
                    int ingredientNum;
                    if (!Int32.TryParse(ingredient, out ingredientNum))
                    {
                        Console.Out.WriteLine("Wrong number.");
                    }
                    else
                    {
                        Console.Out.WriteLine("Enter the amount in grams.");
                        string amount = Console.ReadLine();
                        int amountParsed;
                        if (!Int32.TryParse(amount, out amountParsed))
                        {
                            Console.Out.WriteLine("Invalid number");
                        }
                        else
                        {
                            meal.AddIngredient(_ingredientDatabase.GetIngredient(inputCategory, ingredientNum - 1),
                                    amountParsed);
                        }
                    }
                }
                else
                {
                    Console.Out.WriteLine("Category not found.");
                }
                Console.Out.WriteLine("Enter \"print\" to print all categories or enter a category. When you are done " +
                                          "adding ingredients to meal, enter \"done\".");
                inputCategory = Console.ReadLine();
            }

            meal.SetMealNutrients();
            _currUser.Meals.Add(meal);
            Save();
        }
        
        public void PrintMealsMenu()
        {
            PrintMealsCommands();
            string input = Console.ReadLine();

            while (input != null && !input.ToLower().Equals("6"))
            {
                /* moved this inside of while cycle so it updates more often in case a user stays in this menu for a long time */
                var today = DateTime.Today;
                
                switch (input)
                {
                    case "1":
                        Console.Out.WriteLine("Enter a date in format dd/MM/yyyy.");
                        DateTime temp = _parser.ParseDay(Console.ReadLine());
                        PrintMealsRange(temp, temp);
                        break;
                    case "2":
                        Console.Out.WriteLine("Enter two dates in format dd/MM/yyyy separated by a ':'.");
                        
                        var days = Console.ReadLine();
                        string[] day;
                        if (days != null)
                        {
                            day = days.Split(':');
                        }
                        else
                        {
                            Console.Out.WriteLine("Something went wrong.");
                            break;
                        }
                        PrintMealsRange(_parser.ParseDay(day[0]), _parser.ParseDay(day[1]));
                        break;
                    case "3":
                        PrintMealsAll();
                        break;
                    case "4":
                        var yesterday = today.AddDays(-1);
                        PrintMealsRange(yesterday, yesterday);
                        break;
                    case "5":
                        PrintMealsRange(today, today);
                        break;
                }
                
                PrintMealsCommands();
                input = Console.ReadLine();
            }
        }


        public void CalculateNutrientsMenu()
        {
            CalculateNutrientsCommands();
            string input = Console.ReadLine();
            Ingredient result = new Ingredient();

            while (input != null && !input.ToLower().Equals("6"))
            {
                var today = DateTime.Today;
                switch (input)
                {
                    case "1":
                        Console.Out.WriteLine("Enter a date in format dd/MM/yyyy");
                        DateTime date = _parser.ParseDay(Console.ReadLine());
                        result = CalculateNutrientsRange(date, date);
                        Console.Out.WriteLine("Your intake for " + date.ToString("d") + " was ");
                        PrintNutrients(result);
                        break;
                    case "2":
                        Console.Out.WriteLine("Enter two dates in format dd/MM/yyyy separated by a ':'.");
                        var days = Console.ReadLine();
                        string[] day;
                        if (days != null)
                        {
                            day = days.Split(':');
                        }
                        else
                        {
                            Console.Out.WriteLine("Something went wrong.");
                            break;
                        }
                        var date1 = _parser.ParseDay(day[0]);
                        var date2 = _parser.ParseDay(day[1]);
                        result = CalculateNutrientsRange(date1, date2);
                        result.Divide((date2 - date1).TotalDays + 1);
                        
                        Console.Out.WriteLine("Your average daily intake from " + date1.ToString("d") + " to "
                                              + date2.ToString("d") + " was ");
                        PrintNutrients(result);
                        break;
                    case "3":
                        int daysAmount = _currUser.DaysOnRecord();
                        var nutrientsTotal = CalculateNutrientsRange(DateTime.MinValue, DateTime.MaxValue);
                        nutrientsTotal.Divide(daysAmount);
                        Console.Out.WriteLine("On average your daily intake was ");
                        PrintNutrients(nutrientsTotal);
                        break;
                    case "4":
                        var yesterday = today.AddDays(-1);
                        result = CalculateNutrientsRange(yesterday, yesterday);
                        Console.Out.WriteLine("Your yesterday's intake was ");
                        PrintNutrients(result);
                        break;
                    case "5":
                        result = CalculateNutrientsRange(today, today);
                        Console.Out.WriteLine("Your today's intake was " );
                        PrintNutrients(result);
                        break;
                }
                CalculateNutrientsCommands();
                input = Console.ReadLine();
            }
        }
        
        public void PrintNutrients(Ingredient ingredient)
        {
            Console.Out.WriteLine(ingredient.Calories.ToString("N2") + " calories, " + ingredient.Carbs.ToString("N2")
                                  + "g of carbohydrates, " + ingredient.Fats.ToString("N2") + "g of fats and "
                                  + ingredient.Protein.ToString("N2") 
                                  + "g of protein.");
        }
        
        public void RemoveMealsMenu()
        {
            PrintMealsAll();
            RemoveMealsCommands();
            string input = Console.ReadLine();

            while (input != null && !input.ToLower().Equals("q"))
            {
                int index;
                if (!Int32.TryParse(input, out index))
                {
                    Console.Out.WriteLine("Invalid input, try again.");
                }
                else if(index - 1 >= _currUser.Meals.Count || index - 1 < 0)
                {
                    Console.Out.WriteLine("Index out of range.");
                }
                else
                {
                    _currUser.Meals.RemoveAt(index - 1);
                }
                PrintMealsAll();
                RemoveMealsCommands();
                input = Console.ReadLine();
            }

            Save();
        }

        public void Save()
        {
            Saving.Wait();
            Saving = _userDatabase.SaveUser(_currUser);
        }
        
        
        public void PrintMealsAll()
        {
            PrintMealsRange(DateTime.MinValue, DateTime.MaxValue);
        }

        public void PrintMealsRange(DateTime date1, DateTime date2)
        {
            int counter = 0;
            
            foreach (var meal in _currUser.Meals)
            {
                counter++;
                
                if (meal.Date >= date1 && meal.Date <= date2)
                {
                    Console.Out.WriteLine(meal.Date.ToString("d"));
                    Console.Out.Write(counter + ". ");
                    Console.Out.Write(meal.Name + " contained ");
                    PrintNutrients(meal.MealToIngredient());
                }
            }
        }
        
        
        public Ingredient CalculateNutrientsRange(DateTime date1, DateTime date2)
        {
            Ingredient result = new Ingredient();

            foreach (var meal in _currUser.Meals)
            {
                if (meal.Date >= date1 && meal.Date <= date2)
                {
                    result = result.Add(meal.MealToIngredient());
                }
            }
            return result;
        }
    }
}